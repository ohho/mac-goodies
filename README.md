# Mac Goodies

介紹一些開發相關的工具給用 Mac 的同事⋯⋯



- Terminal，Mac 自帶，在 Applications > Utilities > Terminal

- Homebrew，將一堆極度好用的 UNIX 工具，從 Linux 移植到 Mac

  https://brew.sh/

- node.js 有得多版本，用 nvm 管理，挺方便的

  https://github.com/creationix/nvm

  https://stackoverflow.com/questions/28017374/what-is-the-suggested-way-to-install-brew-node-js-io-js-nvm-npm-on-os-x
  
- MySQL，安裝了 MySQL 之後，總需要一個好用的前端，我見 Taylor Otwell 都在用 Sequel Pro

  https://www.sequelpro.com/

  https://dev.mysql.com/doc/mysql-osx-excerpt/5.7/en/osx-installation.html

- Editor，有人說 editor 像宗教，我在用

  https://code.visualstudio.com/

  https://macromates.com/

  https://www.barebones.com/products/bbedit/download.html
  
  看出共通性了嗎？都是免費的 XD

- 在 Windows 喜歡弄 registry 的，看這裡

  https://www.defaults-write.com/

  https://www.defaults-write.com/speed-up-macos-high-sierra/

- 壓縮軟件

  https://www.keka.io/en/

- 虛擬機

  https://www.virtualbox.org/

- 如果懷念 Windows，譬如要去跑個 Visual Basic app 之類的

  https://itunes.apple.com/us/app/microsoft-remote-desktop-8/id715768417?mt=12

